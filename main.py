import time
import random

class Timer:
    def __init__(self, name):
        self.name = name

    def __enter__(self):
        self.start = time.time()
        print(f'Алгоритм: {self.name}')
        time_str = time.strftime("%H.%M.%S (%Y-%m-%d)", time.gmtime(self.start))
        print(f'Начало: {time_str}')

    def __exit__(self, *args):
        self.end = time.time()
        time_str = time.strftime("%H.%M.%S (%Y-%m-%d)", time.gmtime(self.end))
        print(f'Окончание: {time_str}')
        self.interval = self.end - self.start
        print(f'Время выполнения: {self.interval:0.5f} (секунд)')
        return False

random_list = [random.randint(1, 100) for _ in range(1000)]

def bubble_sort(random_list):
  li = random_list.copy()
  n = 1
  while n < len(li):
      for i in range(len(li) - n):
            if li[i] > li[i + 1]:
                li[i], li[i + 1] = li[i + 1], li[i]
      n += 1
  
  return li

with Timer('sorted') as t:
  sorted_list = sorted(random_list)

print()
with Timer('bubble sort') as t:
  bubble_list = bubble_sort(random_list)

# print(random_list)
# print(sorted_list)
# print(bubble_list)
# print(sorted_list == bubble_list)